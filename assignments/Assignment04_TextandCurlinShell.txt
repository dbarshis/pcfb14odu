Task, Using only the command line (can use JEdit to view file formats but not for anything else) tell me on how many lines the weather was partly cloudy at the ORF airport in August 2014 according to weather underground.
Bonus tasks, tell me how on how many lines the temperature was between 80.0 and 89.9, and between 90.0 and 99.9
Steps:
	1. Download via "curl" all the data for August 2014 from ORF airport to your sandbox directory (check out pcfb/scripts/shellscripts.sh if you don't have the book)
	2. use grep to count the number of occurrences of partly cloudly or the temp ranges
	3. save your "history" to a file named YourName_PCfB_Assign4_workflow.txt and email me the file and include the results (hours of Partly Cloudy weather and temp ranges if you can) in the body of the email.

Bonus bonus task (really only do this if the first steps were super easy)
How does this August compare with 2010-2013 in terms of Partly Cloudy weather and/or hotter temps?