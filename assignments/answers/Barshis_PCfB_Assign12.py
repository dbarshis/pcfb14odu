#! /usr/bin/env python

import sys

files=sys.argv[1:]

Masterlist=[]
for file in files:
	infile = open(file, 'r')
	linecount=0
	for line in infile:
		if linecount > 0: #to skip header line
			cols=line.rstrip().split(',')
			Masterlist.append('%s\t%s\t%s' %(cols[0],cols[2],cols[1]))
		linecount+=1
	infile.close()

outfile = open('Barshis_Assign12_output.txt','w')
outfile.write('rovCtdDtg\tdepth\tvehicle\n') # to write header to output file

Masterlist.sort() #to sort into order
for item in Masterlist:
	outfile.write('%s\n'%(item))
outfile.close()