# Practical Computing for Biologists FA14

This repository contains the source code, datasets, and documentation for 
Dan Barshis' Practical Computing for Biologists course (Biology 1425) at Old Dominion University, as 
taught in the fall of 2014.

See the [syllabus](https://bitbucket.org/dbarshis/pcfb14odu/src/master/syllabus/Biol795-895_PCB_Fall_2014_Barshis.pdf) 
for additional information.

The book for the class is:

Haddock, S. H. D. and Dunn, C. W. (2010). Practical Computing for Biologists. 
[Sinauer Associates](http://practicalcomputing.org). Required.

Here are some of the appendices from the book, which summarize frequently used 
commands:
[Appendices](http://practicalcomputing.org/files/PCfB_Appendices.pdf)

## Pre-class to-do's

  - Fill out pre-class [questionnaire](https://bitbucket.org/dbarshis/pcfb14odu/src/master/pre-classquestionnaire.txt) 
  and email to dan as a .txt file (suggest using textwrangler on mac, notepad++ on Windows, or gedit on Ubuntu VM if you can set it up)
  - Install the software mentioned below
  - Request VPN access asap
  - buy the book
  - run through a basic unix tutorial (google "basic unix tutorial") [this one looks good - do as many as can](http://www.ee.surrey.ac.uk/Teaching/Unix/)
  
## Assignments

- In progress, check back for updates

## Software to install on your own computer

A laptop will be required for all in-class sessions. You will use the following programs
to work with data on your laptop.

### For all computers:

- [VPN](http://occs.odu.edu/gettingconnected/vpn.shtml). Everyone will need to 
register for off-campus vpn access to be able to access the course wiki. Here is
the document explaining how to do it [vpninfo](https://bitbucket.org/dbarshis/pcfb14odu/src/master/softwareresources/ODU_campus_VPN_access_procedure.pdf). Essentially you'll need to 
request the account access via the universal account request form, then you'll need
to complete the online safety training, and download the software. If you already 
have access, try and view this web-page 
[pcfbwiki](http://128.82.116.111/dokuwiki/doku.php?id=start), 
it should say "access denied".

- [VirtualBox](https://www.virtualbox.org/). Please download and install the VirtualBox
distribution appropriate for your main operating system as well as the 
[extension pack](http://download.virtualbox.org/virtualbox/4.3.14/Oracle_VM_VirtualBox_Extension_Pack-4.3.14-95030.vbox-extpack)
Next, download the virtual disk image posted [here](https://www.dropbox.com/s/un8azh3u9jha19t/pcfbodu14.ova?dl=0) and import into 
virtualbox following these instructions:
- open virtual box
- click on File>Import Appliance	

- [R](http://www.r-project.org)

### For OS X (ie, Macs):

- [TextWrangler](http://www.barebones.com/products/textwrangler/)

### For Microsoft Windows:

- [Notepad++](http://notepad-plus-plus.org)

- [PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html)


##External links

Documentation and resources for Turing, the computer cluster we may use for our 
analyses:
[Turing](http://www.odu.edu/facultystaff/research/resources/computing/high-performance-computing)

[software-carpentry](http://software-carpentry.org) has some good tutorials and resources
though they are geared more towards two day intensive workshops

[evomics](http://evomics.org/learning/unix-tutorial/) a really good unix tutorial with a slant towards NGS bioinformatics

## Licenses

All original software is licensed under a 
[GPL v3 license](http://www.gnu.org/licenses/gpl-3.0.html). 
Data that is downloaded from public archives is distributed in accordance with 
the license of the source archive. All other original content is distributed 
under a [Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported 
License](http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US).
